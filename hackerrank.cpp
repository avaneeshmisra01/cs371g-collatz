// --------------
// RunCollatz.cpp
// --------------

// --------
// includes
// --------


#include <iostream> // cin, cout, endl
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <cassert> // assert

using namespace std;

unsigned const int CACHE_SIZE = 1000000;
unsigned int cache[CACHE_SIZE];
// ----
// main
// ----

int main () {
    cache_maker();
    string s;
    while (getline(cin, s)) {
        // ----
        // read
        // ----

        istringstream iss(s);
        unsigned i, j;
        iss >> i >> j;

        // ----
        // eval
        // ----

        unsigned v = max_cycle_length(i, j);

        // -----
        // print
        // -----
        cout << i << " " << j << " " << v << endl;}

    return 0;}



unsigned max_cycle_length(unsigned i, unsigned j) {
    assert(i > 0);
    assert(j > 0);
    //ensure bounds are met
    assert(i < 1000000);
    assert(j < 1000000);
    int start;
    int stop;
    //check which is lower
    if (i < j)
    {
        start = i;
        stop = j;
    }
    else
    {
        start = j;
        stop = i;
    }
    //track max
    unsigned max = 0;

    for (int index = start; index <= stop; index++)
    {
        if (cache[index - 1] > max)
        {
            max = cache[index - 1];
        }
    }

    assert(max > 0);
    return max;
}

//create cache

void cache_maker()
{
    for (unsigned i = 1; i < CACHE_SIZE; i++)
    {

        //use long to avoid overflow, unsigned
        unsigned long num = i;
        int cycleLength = 1;
        while (num > 1)
        {
            //check if num is already cached
            if ((num - 1) < CACHE_SIZE && cache[num - 1] != 0)
            {
                cycleLength += cache[num - 1] - 1;
                break;
            }
            if ((num % 2) == 0)
            {
                num = (num / 2);
                ++cycleLength;
            }
            else
            {
                //optimization for odd numbers
                num = num + (num >> 1) + 1;
                ++++cycleLength;
            }
        }
        //fill cache with newfound cyclelength

        cache[i - 1] = cycleLength;
    }
}
