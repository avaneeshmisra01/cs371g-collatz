// -----------
// Collatz.hpp
// -----------

#ifndef Collatz_hpp
#define Collatz_hpp

// ----------------
// max_cycle_length
// ----------------

/**
 * @param two positive ints
 * @return one positive int
 */
unsigned max_cycle_length (unsigned i, unsigned j);

/**
* @param none
* @return none
*/
void cache_maker ();

#endif // Collatz_hpp
