// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert

#include "Collatz.hpp"
#include <array>



// ----------------
// max_cycle_length
// ----------------
unsigned const int CACHE_SIZE = 1000000;
std::array<unsigned int, CACHE_SIZE> cache;

unsigned max_cycle_length(unsigned i, unsigned j)
{
    assert(i > 0);
    assert(j > 0);
    //ensure bounds are met
    assert(i < 1000000);
    assert(j < 1000000);
    int start;
    int stop;
    //check which is lower in order to find start index
    if (i < j)
    {
        start = i;
        stop = j;
    }
    else
    {
        start = j;
        stop = i;
    }
    //track max cycle length
    unsigned max = 0;

    //ensures that the max will be taken from cache if it is larger
    for (int index = start; index <= stop; index++)
    {
        if (cache[index - 1] > max)
        {
            max = cache[index - 1];
        }
    }

    assert(max > 0);
    return max;
}

//create cache
void cache_maker()
{
    for (unsigned i = 1; i < CACHE_SIZE; i++)
    {

        //use long to avoid overflow, unsigned
        unsigned long num = i;
        int cycleLength = 1;
        while (num > 1)
        {
            //check if num is already cached
            //This implementation checks the cache before any arithmetic 
            if ((num - 1) < CACHE_SIZE && cache[num - 1] != 0)
            {
                cycleLength += cache[num - 1] - 1;
                break;
            }
            if ((num % 2) == 0)
            {
                num = (num / 2);
                ++cycleLength;
            }
            else
            {
                //optimization for odd numbers
                num = num + (num >> 1) + 1;
                ++++cycleLength;
            }
        }
        //fill cache with newfound cyclelength
        //Done at end so that number's calculation is entered into cache
        cache[i - 1] = cycleLength;
    }
}
